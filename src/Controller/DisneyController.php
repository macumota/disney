<?php

namespace App\Controller;

use App\Entity\Characters;
use App\Form\NewCharacterType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DisneyController extends AbstractController {

    #[Route('/charactersList', name:"charactersList")]
    public function getCharactersList(EntityManagerInterface $doctrine, Request $request){
        $repositorio=$doctrine->getRepository(Characters::class);
        $characters=$repositorio->findAll();
        return $this -> render("characters/charactersList.html.twig", ["characters" => $characters]);
    }

    #[Route('/characterDetail/{id}', name:"characterDetail")]
    public function getCharacterDetail(EntityManagerInterface $doctrine, $id){
        $repositorio=$doctrine->getRepository(Characters::class);
        $character=$repositorio->find($id);
        return $this -> render("characters/characterDetail.html.twig", ['character'=> $character]);
    }

    #[Route('/new/character', name:"newCharacters")]
    public function createCharacters(EntityManagerInterface $doctrine){
        $character1= new Characters();
       $character1->setNombre("Cenicienta");
       $character1->setTipo("Princesa");
       $character1->setPelicula ("La Cenicienta");
       $character1->setImagen("https://a-desk.org/wp-content/uploads/2015/11/cenicienta-limpiando__a-desk-595x400.jpg");
       $character1->setLocalizacion("Alemania");

       $character2= new Characters();
       $character2->setNombre("Hércules");
       $character2->setTipo("Héroe");
       $character2->setPelicula("Hércules");
       $character2->setImagen("https://img2.freepng.es/20190126/qwt/kisspng-heracles-hercules-clip-art-openclipart-image-hercules-5c4d1b59377938.7330523015485571452272.jpg");
       $character2->setLocalizacion("Grecia");

       $character3= new Characters();
       $character3->setNombre("Mike Wazowski");
       $character3->setTipo("Monstruo");
       $character3->setPelicula("Monstruos S.A.");
       $character3->setImagen("https://img.fruugo.com/product/8/43/7700438_max.jpg");
       $character3->setLocalizacion("Monstruópolis");

       $character4= new Characters();
       $character4->setNombre("Stich");
       $character4->setTipo("Extraterrestre");
       $character4->setPelicula("Lilo & Stich");
       $character4->setImagen("https://upload.wikimedia.org/wikipedia/en/thumb/d/d2/Stitch_%28Lilo_%26_Stitch%29.svg/640px-Stitch_%28Lilo_%26_Stitch%29.svg.png");
       $character4->setLocalizacion("Hawai");

       $character5= new Characters();
       $character5->setNombre("Woody");
       $character5->setTipo("Juguete");
       $character5->setPelicula("Toy Story");
       $character5->setImagen("https://media.vandalsports.com/i/1706x960/7-2022/202277162619_1.jpg.webp");
       $character5->setLocalizacion("Estados Unidos");

       $character6= new Characters();
       $character6->setNombre("Jafar");
       $character6->setTipo("Villano");
       $character6->setPelicula("Aladdin");
       $character6->setImagen("https://hips.hearstapps.com/es.h-cdn.co/fotoes/images/peliculas-para-ninos-cine-infantil/personajes-disney-orgullo-lgtbi/jafar-aladdin/138118167-1-esl-ES/Jafar-Aladdin.jpg");
       $character6->setLocalizacion("Arabia");

       $character7= new Characters();
       $character7->setNombre("Úrsula");
       $character7->setTipo("Villana");
       $character7->setPelicula("La Sirenita");
       $character7->setImagen("https://eltallerdehector.com/wp-content/uploads/2022/06/943eb-personajes-de-la-sirenita-png-ursula.png");
       $character7->setLocalizacion("Atlántida");

       $doctrine->persist($character1);
       $doctrine->persist($character2);
       $doctrine->persist($character3);
       $doctrine->persist($character4);
       $doctrine->persist($character5);
       $doctrine->persist($character6);
       $doctrine->persist($character7);

       $doctrine->flush();
       return new Response("Personajes insertados correctamente");
    }

    #[Route('/insert/character', name:"insertCharacter")]
    public function insertCharacter(Request $request ,EntityManagerInterface $doctrine){

        $form = $this -> createForm(NewCharacterType::class);

        $form-> handleRequest($request);

        if($form -> isSubmitted()&& $form -> isValid()){
            $character = $form -> getData();
            $doctrine -> persist($character);
            $doctrine -> flush();
        }

        return $this-> renderForm("/characters/createCharacter.html.twig", ["createForm"=>$form]);

        $this->addFlash('success', 'Personaje creado correctamente');
    }

    #[Route('/delete/character/{id}', name:'deleteCharacter')]
    public function deleteCharacter(EntityManagerInterface $doctrine, $id){
        $repositorio=$doctrine->getRepository(Characters::class);
        $character=$repositorio->find($id);
        $doctrine->remove($character);
        $doctrine->flush();
        $this->addFlash('success', 'Personaje borrado correctamente');
        return $this-> redirectToRoute('charactersList');
    }
}