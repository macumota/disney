<?php

namespace App\Form;

use App\Entity\Characters;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewCharacterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('Nombre', TextType::class,
            ['attr' => ['placeholder' => 'Ej. Hades']])
            ->add('Pelicula', TextType::class, ["label"=>"Película", 'attr' => ['placeholder' => 'Ej. Hércules']])
            ->add('Tipo', TextType::class,
            ['attr' => ['placeholder' => 'Ej. Villano']])
            ->add('Localizacion', TextType::class, ["label"=>"Localización", 'attr' => ['placeholder' => 'Ej. Inframundo']])
            ->add('Imagen', TextType::class,
            ['attr' => ['placeholder' => 'Introduce una URL']])
            ->add('Enviar', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Characters::class,
        ]);
    }
}
