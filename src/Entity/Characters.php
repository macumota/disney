<?php

namespace App\Entity;

use App\Repository\CharactersRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CharactersRepository::class)]
class Characters
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 150)]
    private ?string $Nombre = null;

    #[ORM\Column(length: 250, nullable: true)]
    private ?string $Pelicula = null;

    #[ORM\Column(length: 150, nullable: true)]
    private ?string $Tipo = null;

    #[ORM\Column(length: 150, nullable: true)]
    private ?string $Localizacion = null;

    #[ORM\Column(length: 255)]
    private ?string $Imagen = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->Nombre;
    }

    public function setNombre(string $Nombre): self
    {
        $this->Nombre = $Nombre;

        return $this;
    }

    public function getPelicula(): ?string
    {
        return $this->Pelicula;
    }

    public function setPelicula(string $Pelicula): self
    {
        $this->Pelicula= $Pelicula;

        return $this;
    }

    public function getTipo(): ?string
    {
        return $this->Tipo;
    }

    public function setTipo(?string $Tipo): self
    {
        $this->Tipo = $Tipo;

        return $this;
    }

    public function getLocalizacion(): ?string
    {
        return $this->Localizacion;
    }

    public function setLocalizacion(?string $Localizacion): self
    {
        $this->Localizacion = $Localizacion;

        return $this;
    }

    public function getImagen(): ?string
    {
        return $this->Imagen;
    }

    public function setImagen(string $Imagen): self
    {
        $this->Imagen = $Imagen;

        return $this;
    }
}
